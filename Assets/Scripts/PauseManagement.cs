using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManagement : MonoBehaviour
{
    public static bool isGamePaused = false;
    [SerializeField]private MyCursor cursor;
    public GameObject PauseMenuUI;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isGamePaused)
            {
                Resume();
            } else
            {
                Paused();
            }
        }
    }

    public void Resume()
    {
        PauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        isGamePaused = false;
        cursor.ChangeCursor(1);
    }

    void Paused()
    {
        PauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        Cursor.visible = false;
        isGamePaused = true;
        cursor.ChangeCursor(0);
    }
}

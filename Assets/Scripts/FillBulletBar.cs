using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngine.UI;

public class FillBulletBar : MonoBehaviour
{
    public Image fillImage;
    private Slider _slider;
    public Text textBullets;
    public Player player = new();

    void Awake()
    {
        _slider = GetComponent<Slider>();
    }
    void Update()
    {
        if (_slider.value <= _slider.minValue) fillImage.enabled = false;
        if(_slider.value > _slider.minValue && !fillImage.enabled) fillImage.enabled = true;
        
        float fillValue = player.currentBullets / player.bullets;

        textBullets.text = "Bullets: " + player.currentBullets.ToString() + "/" + player.bullets.ToString();

        _slider.value = fillValue;
    }

    
}

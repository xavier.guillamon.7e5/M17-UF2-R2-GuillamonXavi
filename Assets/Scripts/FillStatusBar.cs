using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngine.UI;

public class FillStatusBar : MonoBehaviour
{
    public Image fillImage;
    private Slider _slider;
    public Player player = new();

    void Awake()
    {
        _slider = GetComponent<Slider>();
    }
    void Update()
    {
        if(_slider.value <= _slider.minValue) fillImage.enabled = false;
        if(_slider.value > _slider.minValue && !fillImage.enabled) fillImage.enabled = true;
        
        float fillValue = player._currentHp / player._maxHp;

        if(fillValue <= _slider.maxValue / 3) fillImage.color = UnityEngine.Color.red;
        if (fillValue <= _slider.maxValue / 2 && fillValue > _slider.maxValue / 3) fillImage.color = UnityEngine.Color.yellow;
        if (fillValue > _slider.maxValue / 2) fillImage.color = UnityEngine.Color.green;
        _slider.value = fillValue;
    }

    
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorManager : MonoBehaviour
{
    private GameObject[] doorChildren = new GameObject[5];
    public AudioSource doorOpeningAudio;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.DoorOpen += OpenDoor;

        for (int i = 0; i < transform.childCount; i++)
            doorChildren[i] = transform.GetChild(i).gameObject;

    }
    public void OnDisable()
    {
        GameManager.Instance.DoorOpen -= OpenDoor;
    }
    public void OpenDoor()
    {
        StartCoroutine(Waiter());
    }
    IEnumerator Waiter()
    {
        //Wait for 4 seconds
        yield return new WaitForSeconds(1);
        doorChildren[2].SetActive(false);
        doorChildren[1].SetActive(true);
        doorOpeningAudio.Play();

        yield return new WaitForSeconds(1);
        doorChildren[1].SetActive(false);
        doorChildren[0].SetActive(false);
        doorChildren[3].SetActive(true);

    }
}

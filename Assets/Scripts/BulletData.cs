using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class BulletData : MonoBehaviour
{
    public WeaponData bulletType;
    public Transform bulletBody;
    Vector3 myMousePosition;
    Vector3 bulletDirection;

    private float bulletSpeed;
    //private float bulletDamage;
    private SpriteRenderer spriteRenderer;

    void Start()
    {
        bulletSpeed = bulletType.bulletSpeed; 
        //bulletDamage = bulletType.bulletDamage;
        spriteRenderer = GetComponent<SpriteRenderer>();

        myMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        bulletDirection = new Vector3((myMousePosition.x - bulletBody.transform.position.x), (myMousePosition.y - bulletBody.transform.position.y), 0);
    }
    void Update()
    {
        transform.position += bulletSpeed * bulletDirection.normalized * Time.deltaTime;
        spriteRenderer.sprite = bulletType.bullet;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!collision.CompareTag("Player"))
        {
            Destroy(this.gameObject);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXeffect : MonoBehaviour
{
    public AudioSource Door;
    
    void PlayDoorSound()
    {
        Door.Play();
    }
}

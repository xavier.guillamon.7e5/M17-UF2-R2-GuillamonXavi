using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyData : Character
{
    protected Transform _playerPosition = null;
    void Start()
    {
        _playerPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        anim = GetComponent<Animator>();
        //knockBackTime = 0.4f;
    }
    void Update()
    {
        Move();
    }
    protected override void Move()
    {
        transform.position = Vector2.MoveTowards(transform.position, _playerPosition.position, _speed * Time.deltaTime);
        anim.SetFloat("MovementX", _playerPosition.position.x - transform.position.x);
        anim.SetFloat("MovementY", _playerPosition.position.y - transform.position.y);
    }
}

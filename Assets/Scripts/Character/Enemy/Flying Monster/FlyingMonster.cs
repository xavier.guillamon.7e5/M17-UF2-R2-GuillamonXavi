using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class FlyingMonster : EnemyData, IShooteable
{
    public GameObject bullet;
    public Transform bulletPos;

    private GameObject _player;

    private float cooldownShoot;
    void Start()
    {
        _playerPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        _player = GameObject.FindGameObjectWithTag("Player");
    }
    void Update()
    {
        base.Move();
        float distance = Vector2.Distance(transform.position, _player.transform.position);

        if(distance < 10)
        {
            cooldownShoot += Time.deltaTime; 
            if (cooldownShoot > 1)
            {
                cooldownShoot = 0;
                Shooting();
            }
        }        
    }
    public void Shooting()
    {
        var shoot = Instantiate(bullet, bulletPos.position, Quaternion.identity);
        Destroy(shoot, 1);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("CharacterDamageDealer"))
        {
            Hurt(3, 0.6f);
            Vector2 difference = (transform.position - other.transform.position) ;
            transform.position = new Vector2((transform.position.x + difference.x), (transform.position.y + difference.y));
        }
        if (other.CompareTag("StunWeapon"))
        {
            Debug.Log("speed");
            StartCoroutine(Stun());
        }
    }

    IEnumerator Stun()
    {
        _speed = 0;
        yield return new WaitForSeconds(2f);
        _speed = 2.5f;
    }
}

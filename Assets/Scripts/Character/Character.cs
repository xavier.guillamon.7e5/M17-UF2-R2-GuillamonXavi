using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Character : MonoBehaviour
{
    [SerializeField]
    protected float _speed;
    [SerializeField]
    public float _maxHp;
    public float _currentHp;
    protected Animator anim;
    [SerializeField] protected AudioSource deadSound;

    protected virtual void Move() { }
    protected virtual void Awake()
    {
        anim = GetComponent<Animator>();
        Debug.Log("Speed: " + _speed + " from " + this.name);
    }

    protected virtual void Hurt(float damage, float seconds) 
    {
        _currentHp -= damage;
        if(_currentHp <= 0)
        {
            if (this.gameObject.CompareTag("Player"))
            {
                _speed = 0;
                anim.SetTrigger("IsPlayerDead");
                deadSound.Play();
                StartCoroutine(DeadCoroutine());
            }
            if (this.gameObject.CompareTag("Enemy"))
            {
                if (this.gameObject.name == "GreenMonster")
                {
                    GameManager.Instance.IncreaseCoins(50);
                    GameManager.Instance.SetEnemiesDead();
                }
                if (this.gameObject.name == "FlyingMonster")
                {
                    GameManager.Instance.IncreaseCoins(70);
                    GameManager.Instance.SetEnemiesDead();
                }
                if (this.gameObject.name == "Boss")
                {
                    GameManager.Instance.IncreaseCoins(180);
                    GameManager.Instance.SetEnemiesDead();
                } 
                if (this.gameObject.name == "FinalBoss")
                {
                    GameManager.Instance.IncreaseCoins(300);
                    GameManager.Instance.FinalBossDead();
                } 
                _speed = 0;
                deadSound.Play();
                anim.SetTrigger("IsEnemyDead");
                Destroy(GetComponent<Collider2D>());
                Destroy(this.gameObject, seconds);

            }
        }
        
        IEnumerator DeadCoroutine()
        {
            yield return new WaitForSeconds(0.6f);
            //GameManager.Instance.SaveResultsJson();
            Destroy(this.gameObject, seconds);
            SceneManager.LoadScene("GameOver");

        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : Character, IShooteable
{
    public GameObject bullet;
    public float timeBulletAlive;
    public float currentBullets;
    public float bullets = 15;
    public WeaponData bulletData;

    private Rigidbody2D _rb;
    private Transform body;
    public Animator _animator;

    private float horizontal;
    private float vertical;
    private Vector2 _direction;
    private float cooldownShoot;
    private readonly float cooldownShootTime = 0.2f;

    private float normalSpeed = 10;

    public InventoryObject inventory;
    private int indexInventory = 0;
    [SerializeField] private AudioSource collect;
    [SerializeField] private AudioSource enemyAttack;
    [SerializeField] private AudioSource shoot;
    [SerializeField] private AudioSource teleport;

    void Start()
    {
        _currentHp = _maxHp;
        currentBullets = bullets;
        body = transform;
        _rb = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        cooldownShoot = cooldownShootTime;
        bulletData = gameObject.GetComponent<WeaponData>();
        normalSpeed = _speed;
    }
   
    void Update()
    {
        InputCollection();
        Move();
        Animate();
        cooldownShoot -= Time.deltaTime;
        if (Input.GetMouseButtonDown(0) && currentBullets != 0)
        {
            if (cooldownShoot < 0)
            {
                Shooting();
                cooldownShoot = cooldownShootTime;
            }
            
        }
        if (Input.GetMouseButtonDown(1))
        {
            if (cooldownShoot < 0)
            {
                Dash();
                cooldownShoot = cooldownShootTime;
            }
            _speed = normalSpeed;

        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Recharge();
        }
        SwitchEquip();
    }

    private void Dash()
    {
        _speed *= 3;
    }

    private void InputCollection()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");
        _direction = new Vector2(horizontal, vertical).normalized;
    }

    protected override void Move()
    {
        _rb.velocity = new Vector2(_direction.x * _speed, _direction.y * _speed);
    }

    public void Shooting()
    {
        if (inventory.inventory.Count != 0)
        {
            this.shoot.Play();
            var shoot = Instantiate(bullet, transform.position, Quaternion.identity);
            shoot.GetComponent<BulletData>().bulletBody = transform;
            currentBullets--;
            Destroy(shoot, timeBulletAlive);
        }
        else return;
        
    }
    private void Recharge()
    {
        currentBullets = 15;
    }
    private void SwitchEquip()
    {
        if (inventory.inventory.Count > 1)
            if (Input.GetKeyDown(KeyCode.Space))
            {
                bullet = inventory.inventory[indexInventory].weapon;
                indexInventory++;
            }

        if(indexInventory > inventory.inventory.Count -1) indexInventory = 0;
    }

        private void Animate()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosition -= body.position;
        //mousePosition = mousePosition.normalized;
        _animator.SetFloat("MovementX", mousePosition.x);
        _animator.SetFloat("MovementY", mousePosition.y);
    }
    private void OnCollisionEnter2D(Collision2D collider)
    {
        if (collider.gameObject.CompareTag("EnemyDamageDealer") || collider.gameObject.CompareTag("Enemy"))
        {
            Hurt(3, 0.8f);
            enemyAttack.Play();
        }
        if (collider.gameObject.CompareTag("FinalBoss"))
        {
            Hurt(10, 0.8f);
            enemyAttack.Play();
        }
        if (collider.gameObject.CompareTag("HealPack"))
        {
            Heal(7.5f);
            Destroy(collider.gameObject);
        }
        if (collider.gameObject.CompareTag("NewBullet"))
        {
            var weapon = collider.gameObject.GetComponent<Weapon>();
            if(GameManager.Instance.GetCoins() > 99)
            {
                if (weapon)
                {
                    inventory.AddWeapon(weapon.weapondata, 1);
                    bullet = inventory.inventory[indexInventory].weapon;
                    Destroy(collider.gameObject);
                    currentBullets = 15;
                    GameManager.Instance.DecreaseCoins(100);
                    collect.Play();
                }
            }
        }
        if (collider.gameObject.CompareTag("TeleportToShop"))
        {
            teleport.Play();
            transform.position = new Vector3(-108, -10, 0);
        }
        if (collider.gameObject.CompareTag("TeleportToMonsterLvl"))
        {
            teleport.Play();
            transform.position = new Vector3(-0.17f, -10f, 0);
        }
        if (collider.gameObject.CompareTag("TeleportToFinalMap"))
        {
            teleport.Play();
            transform.position = new Vector3(-50f, 35f, 0);
        }
        if (collider.gameObject.CompareTag("TeleportToMainDoor"))
        {
            teleport.Play();
            transform.position = new Vector3(-50f, -10f, 0);
        }
        
    }
    private void Heal(float amountHeal)
    {
        
        Debug.Log(GameManager.Instance.GetCoins());
        _currentHp += amountHeal;
        if (_currentHp > _maxHp)
        {
            _currentHp = _maxHp;
        }
    }
    void OnApplicationQuit()
    {
        inventory.inventory.Clear();
    }
    public Sprite GetIconWeapon()
    {
        return bulletData.bullet;
    }

    public void OnTriggerEnter2D(Collider2D collider2D)
    {
        if (collider2D.gameObject.CompareTag("Trap"))
        {
            SceneManager.LoadScene("GameWin");
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SpawnManager : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _enemies;
    [SerializeField]
    private GameObject _healPack;
    //[SerializeField] private Text _timer;
    private float _whenItSpawns;
    private float _howMuchTimeToSpawn;
    private int _indexToBoss = 3;
    public int _waves;
    private int _wavesIndex = 1;

    // Start is called before the first frame update
    void Start()
    {
        _whenItSpawns = 4;
        _howMuchTimeToSpawn = _whenItSpawns;
    }

    // Update is called once per frame
    void Update()
    {
        if (_wavesIndex < _waves)
        {
            _howMuchTimeToSpawn -= Time.deltaTime;
            if (_howMuchTimeToSpawn < 0)
            {
                Instantiate(_enemies[0], new Vector3(Random.Range(-8f, 8f), transform.position.y, transform.position.z), new Quaternion(0, 0, 0, 0));
                Instantiate(_enemies[1], new Vector3(Random.Range(-8f, 8f), transform.position.y, transform.position.z), new Quaternion(0, 0, 0, 0));
                _howMuchTimeToSpawn = _whenItSpawns;
                _indexToBoss--;
                if (_indexToBoss == 0)
                {
                    Instantiate(_healPack, new Vector3(Random.Range(-8f, 8f), Random.Range(2f, 15f), transform.position.z), new Quaternion(0, 0, 0, 0));
                    Instantiate(_enemies[2], new Vector3(Random.Range(-8f, 8f), transform.position.y, transform.position.z), new Quaternion(0, 0, 0, 0));
                    _indexToBoss = 3;
                }
                _wavesIndex++;
            }
        } 
        
    }
}

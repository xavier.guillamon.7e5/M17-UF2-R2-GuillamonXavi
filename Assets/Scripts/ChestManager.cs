using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestManager : MonoBehaviour
{
    private GameObject[] bulletsChest = new GameObject[5];
    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
            bulletsChest[i] = transform.GetChild(i).gameObject;

        int chance = Random.Range(0, 100);
        if (chance < 40) bulletsChest[0].SetActive(true);
        else if (chance < 65) bulletsChest[1].SetActive(true);
        else if (chance < 80) bulletsChest[2].SetActive(true);
        else if (chance < 100) bulletsChest[3].SetActive(true);
    }
}

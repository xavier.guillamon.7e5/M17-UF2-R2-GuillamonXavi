using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    [SerializeField] private Slider voluemSlider; 
    // Start is called before the first frame update
    void Start()
    {
        if(!PlayerPrefs.HasKey("musicVolume"))
        {
            voluemSlider.value = PlayerPrefs.GetFloat("musicValue", 1);
        }

        else
        {
            Load();
        }
    }

    public void ChangeVolume()
    {
        AudioListener.volume = voluemSlider.value;
        Save();
    }

    private void Load()
    {
        voluemSlider.value = PlayerPrefs.GetFloat("musicValue");
    }

    private void Save()
    {
        PlayerPrefs.SetFloat("musicVolume", voluemSlider.value);
    }
}

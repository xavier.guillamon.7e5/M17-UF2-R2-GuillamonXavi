using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Bullet Data", menuName = "Bullet Data")]
public class WeaponData : ScriptableObject
{
    public float bulletSpeed;
    public float bulletDamage;
    public Sprite bullet;
}

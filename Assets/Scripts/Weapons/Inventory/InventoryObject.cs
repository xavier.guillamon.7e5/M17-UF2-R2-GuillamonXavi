using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Inventory", menuName = "Inventory")]
public class InventoryObject : ScriptableObject
{
    public List<InventorySlot> inventory = new List<InventorySlot>();
    public void AddWeapon(GameObject _weapon, int _amount)
    {
        bool hasItem = false;
        for(int i = 0; i < inventory.Count; i++)
        {
            if (inventory[i].weapon == _weapon)
            {
                inventory[i].AddAmount(_amount);
                hasItem = true;
                break;
            }
        }
        if(!hasItem)
        {
            inventory.Add(new InventorySlot(_weapon, _amount));
        }
    }

}

[System.Serializable]
public class InventorySlot
{
    public GameObject weapon;
    public int amount;
    public InventorySlot(GameObject _weapon, int _amount)
    {
        weapon = _weapon;
        amount = _amount;
    }

    public void AddAmount(int value)
    {
        amount+= value;
    }
}

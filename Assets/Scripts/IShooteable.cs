using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IShooteable
{
    public void Shooting();
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    [SerializeField]
    private int _enemiesDownCounter = 0;
    [SerializeField]
    private int _coins = 0;
    [SerializeField]
    private Text Coins;
    private int _score = 0;

    public int _totalEnemies;
    public event Action DoorOpen = delegate { };
    public event Action TrapAppear = delegate { };

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("GameManager is NULL");
            }
            return _instance;
        }
    }
    private void Awake()
    {
        if (_instance != null && _instance != this) Destroy(this.gameObject);
        _instance = this;
        DontDestroyOnLoad(this.gameObject);
        _coins = 100;
        Coins = GameObject.Find("CoinsText").GetComponent<Text>();
        Coins.text = ""+ _coins;
    }
    public void IncreaseCoins(int points)
    {
        _coins += points;
        Coins.text = ""+ _coins;
        _score += points;
    }

    public int GetCoins()
    {
        return _coins;
    }

    public void DecreaseCoins(int amountCoinsWasted)
    {
        _coins -= amountCoinsWasted;
        Coins.text = ""+ _coins;
    }
    public int GetEnemiesCounter()
    {
        return _enemiesDownCounter;
    }
    public int GetEnemiesDead()
    {
        return _enemiesDownCounter;
    }
    public void CheckEnemiesDead()
    {
        if (_enemiesDownCounter >= _totalEnemies)
        {
            DoorOpen.Invoke();
        }
    }

    public void FinalBossDead()
    {
        TrapAppear.Invoke();
    }
    public void SetEnemiesDead()
    {
        _enemiesDownCounter++;
        CheckEnemiesDead();
    } 
    /*public void SaveResultsJson()
    {
        SaveToJson saveResults = new SaveToJson();
        saveResults.SaveIntoJson(_score);
    }*/


}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class MyCursor : MonoBehaviour
{
    private SpriteRenderer sr;
    [SerializeField] private Sprite[] cursors;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var cursor in cursors)
            Debug.Log(cursor);
        sr = GetComponent<SpriteRenderer>();
        ChangeCursor(1);
       
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 mouseCursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = mouseCursorPos;
    }
    public void ChangeCursor(int indexCursor)
    {
        sr.sprite = cursors[indexCursor];
    }
}

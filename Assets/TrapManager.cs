using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapManager : MonoBehaviour
{

    private GameObject[] _trapChildrens = new GameObject[2];

    public AudioSource doorOpeningAudio;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.TrapAppear += TrapAppears;
         for (int i = 0; i < transform.childCount; i++)
             _trapChildrens[i] = transform.GetChild(i).gameObject;
    }

    public void OnDisable()
    {
        GameManager.Instance.TrapAppear -= TrapAppears;
    }

    public void TrapAppears()
    {
        StartCoroutine(Waiter());
    }

    IEnumerator Waiter()
    {
        //Wait for 4 seconds
        yield return new WaitForSeconds(1);
        _trapChildrens[0].SetActive(true);
        _trapChildrens[1].SetActive(false);
        doorOpeningAudio.Play();
    }
}



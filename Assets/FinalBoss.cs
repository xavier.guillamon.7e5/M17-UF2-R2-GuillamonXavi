using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class FinalBoss : EnemyData
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("CharacterDamageDealer"))
        {
            Hurt(3, 0.7f);
            Vector2 difference = (transform.position - other.transform.position) ;
            transform.position = new Vector2((transform.position.x + difference.x), (transform.position.y + difference.y));
        }
        if (other.CompareTag("FinalWeapon"))
        {
            Hurt(25, 0.7f);
            Vector2 difference = (transform.position - other.transform.position) ;
            transform.position = new Vector2((transform.position.x + difference.x), (transform.position.y + difference.y));
        }
        if (other.CompareTag("StunWeapon"))
        {
            Debug.Log("speed");
            StartCoroutine(Stun());
        }
    }

    IEnumerator Stun()
    {
        _speed = 0;
        yield return new WaitForSeconds(2f);
        _speed = 4;
    }
}